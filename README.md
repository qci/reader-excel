### Lector de bases de datos en excel

#### El archivo excel debe estar conformado de la siguiente manera:
```
en la fila 1 van todos los atributos de la instancia, separadas por cada celda
```

#### Configuracion
En la carpeta de Configuration/ va toda la configuracion de el sistema
```
init.txt => solo lleva una palabra (el nombre del archivo a leer, que debe estar en la carpeta Files/)

dependiendo de el texto que lea en el arhivo init.txt es el archivo que va a crear o a configurar

ej: En el archivo init.txt esta escrito income.xls
entonces va a buscar si existe dentro de la carpeta Configuration un archivo llamado:

income_configuration.txt: que es donde se guardan las expresiones regulares por cada atributo separada por un arroba(@)

en caso de no existir se creara y se debera configurar las expresiones regulares, ya sea por el archivo o en el navegador donde dice configurar.
```

<?php

require_once("vendor/phpoffice/phpexcel/Classes/PHPExcel.php");
require_once("Configuration.php");

class Reader
{
    protected $sheet;
    private   $reader;
    private   $configuration;
    private   $filename;
    public    $class;
    private   $filetype;
    private   $attributes;

    public function __construct($filename)
    {
        $this->configuration = new Configuration(explode('.', $filename)[0]);

        $this->filename = trim($filename);
        $this->filetype = PHPExcel_IOFactory::identify('Files/' . $this->filename);

        $excel  = PHPExcel_IOFactory::createReader($this->filetype);
        $reader = $excel->load('Files/' . $this->filename);

        $this->reader = $reader;

        $this->sheet = $reader->getSheet(0);
        $this->setAttributes();
        $this->setClass();
    }

    public function setClass()
    {
        $class = '';

        foreach($this->configuration->getConfiguration() as $key => $config)
        {
            if(trim($config['type']) === 'clase') {
                $class = $key;
            }
        }

        $this->class = $class;
    }

    public function setAttributes()
    {
        $columns    = [];
        $lastColumn = $this->sheet->getHighestColumn();

        for ($row = 1; $row <= 1; $row++)
        {
            for ($column = 'A'; $column <= $lastColumn; $column++)
            {
                $columns[$column] = $this->sheet->getCell($column.$row)->getValue();
            }
        }
        $this->attributes = $columns;
    }

    public function getAttributes()
    {
        $attrs = [];
        foreach($this->attributes as $key => $attr)
        {
            if($this->class !== $attr)
            {
                $attrs[$key] = $attr;
            }
        }
        return $attrs;
    }

    public function getColumn($attrName)
    {
        foreach($this->attributes as $key => $value)
        {
            if($attrName === $value)
            {
                return $key;
            }
        }
    }

    public function getData($attribute, $sort, $statistics, $show)
    {
        $columns    = [];
        $frecuency  = [];
        $all        = [];
        $column     = $this->getColumn($attribute);
        $lastColumn = $this->sheet->getHighestColumn();
        $lastRow    = $this->sheet->getHighestRow();

        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();
            if(gettype($value) === 'double' || gettype($value) === 'integer')
            {
                $columns[] = $value;
            }
            $all[] = $value;
            $this->setFrecuency($frecuency, $value, $attribute, $show);
        }

        $statistic = $this->getStatistic($columns, $frecuency, $all, $statistics, $sort);

        $data = [];
        $data['frecuency'] = $frecuency;
        $data['statistic'] = $statistic;

        return $data;
    }

    public function getStatistic(&$columns, $frecuency, $all, $statistics, $sort)
    {
        $data    = [];
        $lastRow = $this->sheet->getHighestRow();
        ($sort === 'ASC') ? sort($columns) : asort($columns);
        if(count($columns))
        {
            $data['min'] = min($columns);
            $data['max'] = max($columns);
            $data['desviacion'] = $this->standard_deviation($all);
            $data['media'] = array_sum($columns) / ($lastRow-1);
            $data['mediana'] = $columns[(int)(count($columns)/2)];
            $data['moda'] = $this->getFashion($frecuency);
        }

        return $data;
    }

    public function getFashion($frecuency)
    {
        $fashion = [];
        foreach($frecuency as $key => $value)
        {
            if($value === max($frecuency))
            {
                $fashion[] = $key;
            }
        }

        return $fashion;
    }

    function standard_deviation($values, $sample = false)
    {
        $mean = array_sum($values) / count($values);
        $variance = 0.0;
        foreach ($values as $i)
        {
            $variance += pow($i - $mean, 2);
        }
        $variance /= ( $sample ? count($values) - 1 : count($values) );
        return (float) sqrt($variance);
    }

    public function setFrecuency(&$array, $value, $column, $show)
    {
        if(!is_null($column) && $show === 'all')
        {
            $this->setValue($array, trim($value), $column);
        }
        else
        {
            if(!is_null($column) && !$this->configuration->valid($column, trim($value)))
            {
                $this->setValue($array, trim($value), $column);
            }
        }
    }

    public function setValue(&$array, $value, $column = NULL)
    {
        if(!array_key_exists((string)$value, $array))
        {
            $array[$value]['value'] = 1;
        }
        else
        {
            $array[$value]['value'] = $array[$value]['value']+1;
        }

        if(!count($this->configuration->getConfiguration()))
        {
            die('primero debes configurar tus expresiones regulares, ve a configurar');
        }

        if(count($this->configuration->getConfiguration()) && trim($this->configuration->getConfiguration()[$column]['type']) === 'nominal')
        {
            $array[$value]['sim'] = $this->getLevenshtein($value, $column);
        }
        else
        {
            $array[$value]['sim'] = '';
        }
    }

    public function getLevenshtein($value, $column)
    {
        $values = $this->getCorrectValues($column);
        if(count($values))
        {
            $lev    = 0;
            for($i=0; $i<count($values); $i++)
            {
                $distancy1 = levenshtein(trim($value), trim($values[$lev]));
                $distancy2 = levenshtein(trim($value), trim($values[$i]));

                if($distancy1 > $distancy2)
                {
                    $lev = $i;
                }
                if($distancy1 === 0 || $distancy2 === 0){
                    return '';
                }
            }
            return $values[$lev];
        }
        return '';
    }

    public function getCorrectValues($attribute)
    {
        $column     = $this->getColumn($attribute);
        $lastRow    = $this->sheet->getHighestRow();
        $array      = [];

        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();

            if($this->configuration->valid($attribute, $value))
            {
                if(!in_array($value, $array))
                {
                    $array[] = $value;
                }
            }
        }

        return $array;
    }

    public function getConfiguration()
    {
        return $this->configuration->getConfiguration();
    }

    public function getCorrelation($attr1, $attr2)
    {
        $matrix     = [];
        $values1    = [];
        $values2    = [];
        $column1    = $this->getColumn($attr1);
        $column2    = $this->getColumn($attr2);

        $lastRow    = $this->sheet->getHighestRow();

        for ($row = 2; $row <= $lastRow; $row++)
        {
            $values1[] = $this->sheet->getCell($column1.$row)->getValue();
            $values2[] = $this->sheet->getCell($column2.$row)->getValue();
        }

        for($j=0; $j<count($values2); $j++)
        {
            $key1 = trim($values1[$j]);
            $key2 = trim($values2[$j]);
            if(!isset($matrix[$key1 . ',' . $key2]))
            {
                $matrix[$key1 . ',' . $key2] = (int)1;
            }
            else
            {
                $matrix[$key1 . ',' . $key2] = (int)$matrix[$key1 . ',' . $key2]+1;
            }
        }

        return $matrix;
    }

    public function pearsonFunction($attr1, $attr2)
    {
        $column1    = $this->getColumn($attr1);
        $column2    = $this->getColumn($attr2);
        $lastRow    = $this->sheet->getHighestRow();
        $statistic1 = $this->getValuesStatistics($attr1);
        $statistic2 = $this->getValuesStatistics($attr2);
        $sum        = 0;

        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value1 = (float)$this->sheet->getCell($column1.$row)->getValue();
            $value2 = (float)$this->sheet->getCell($column2.$row)->getValue();

            $value1 -= $statistic1['media'];
            $value2 -= $statistic2['media'];
            $sum += ($value1)*($value2);

        }

        $sum /= (($lastRow-1) * $statistic1['desviacion'] * $statistic2['desviacion']);

        return $sum;
    }

    public function degreeOfFreedom()
    {
        $lastRow    = $this->sheet->getHighestRow();

        return (($lastRow-2)*($lastRow-2));
    }

    public function getValuesStatistics($attribute)
    {
        $columns    = [];
        $frecuency  = [];
        $all        = [];
        $column     = $this->getColumn($attribute);
        $lastRow    = $this->sheet->getHighestRow();

        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();
            if(gettype($value) === 'double' || gettype($value) === 'integer')
            {
                $columns[] = $value;
            }
            $all[] = $value;
            $this->setFrecuency($frecuency, $value, $attribute, 'all');
        }

        $statistic = $this->getStatistic($columns, $frecuency, $all, 'all', 'ASC');

        return $statistic;
    }

    public function getHighestRow()
    {
        return $this->sheet->getHighestRow();
    }

    public function getCell($cell)
    {
        return $this->sheet->getCell($cell)->getValue();
    }

    public function changeValues($valuesToChange, $attrToChange, $filename)
    {
        $column     = $this->getColumn($attrToChange);
        $lastRow    = $this->sheet->getHighestRow();

        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();
            if($valuesToChange === 'all')
            {
                if(!$this->configuration->valid($attrToChange, $value))
                {
                    $new_value = $this->getLevenshtein($value, $attrToChange);
                    $this->sheet->setCellValue($column.$row, $new_value);
                }
            }
            else
            {
                if(trim($valuesToChange) == trim($value))
                {
                    $new_value = $this->getLevenshtein($value, $attrToChange);
                    $this->sheet->setCellValue($column.$row, $new_value);
                }
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($this->reader, 'Excel5');
        $objWriter->save('Files/' . $filename);

    }

    public function min_max($attribute, $min, $max, $filename)
    {
        $column  = $this->getColumn($attribute);
        $lastRow = $this->sheet->getHighestRow();
        $statistic = $this->getValuesStatistics($attribute);
        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();
            $new_value = ($value - $statistic['min']) / ($statistic['max'] - $statistic['min']);
            $new_value = $new_value * (($max - $min) + $min);
            $this->sheet->setCellValue($column.$row, $new_value);
        }

        $objWriter = PHPExcel_IOFactory::createWriter($this->reader, 'Excel5');
        $objWriter->save('Files/' . $filename);
    }

    public function z_score($attribute, $filename)
    {
        $column  = $this->getColumn($attribute);
        $lastRow = $this->sheet->getHighestRow();
        $statistic = $this->getValuesStatistics($attribute);
        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();
            $new_value = ($value - $statistic['media']) / ($statistic['desviacion']);
            $this->sheet->setCellValue($column.$row, $new_value);
        }

        $objWriter = PHPExcel_IOFactory::createWriter($this->reader, 'Excel5');
        $objWriter->save('Files/' . $filename);
    }

    public function z_score_Abs($attribute, $filename)
    {
        $column  = $this->getColumn($attribute);
        $lastRow = $this->sheet->getHighestRow();
        $statistic = $this->getValuesStatistics($attribute);
        $desvAbs = $this->getDesvStdAbs($attribute);
        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();
            $new_value = ($value - $statistic['desviacion']) / $desvAbs;
            $this->sheet->setCellValue($column.$row, $new_value);
        }

        $objWriter = PHPExcel_IOFactory::createWriter($this->reader, 'Excel5');
        $objWriter->save('Files/' . $filename);
    }

    public function getDesvStdAbs($attr)
    {
        $column  = $this->getColumn($attr);
        $lastRow = $this->sheet->getHighestRow();
        $statistic = $this->getValuesStatistics($attr);
        $sum = 0;
        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();
            $sum += $value - ($statistic['desviacion']);
        }

        return $sum / ($lastRow-1);
    }

    public function escalamiento_decimal($attribute, $filename)
    {
        $column  = $this->getColumn($attribute);
        $lastRow = $this->sheet->getHighestRow();
        $statistic = $this->getValuesStatistics($attribute);
        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();
            $new_value = $value / $this->scalar($value);
            $this->sheet->setCellValue($column.$row, $new_value);
        }

        $objWriter = PHPExcel_IOFactory::createWriter($this->reader, 'Excel5');
        $objWriter->save('Files/' . $filename);
    }

    public function scalar($value)
    {
        $len = strlen(abs($value));

        return pow(10, $len);
    }

    public function oneR($attr)
    {
        $class   = $this->getColumn($this->class);
        $column  = $this->getColumn($attr);
        $lastRow = $this->sheet->getHighestRow();
        $frecuency = array();

        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value  = trim($this->sheet->getCell($column.$row)->getValue());
            $valueC = trim($this->sheet->getCell($class.$row)->getValue());

            if(!isset($frecuency[$attr]))
            {
                $frecuency[$attr] = array();
            }

            if(!isset($frecuency[$attr][$value]))
            {
                $frecuency[$attr][$value] = array();
            }

            if(!isset($frecuency[$attr][$value][$valueC]))
            {
                $frecuency[$attr][$value][$valueC] = array();
                $frecuency[$attr][$value][$valueC]['values'] = 1;
                if(trim($valueC)  === '>50K' || trim($valueC)  === 'no')
                {
                    $frecuency[$attr][$value][$valueC]['errors'] = 1;
                }

                $frecuency[$attr][$value][$valueC]['total'] = 1;
            }
            else
            {
                $frecuency[$attr][$value][$valueC]['values'] = $frecuency[$attr][$value][$valueC]['values'] + 1;
                if(trim($valueC)  === '>50K' || trim($valueC)  === 'no')
                {
                    $frecuency[$attr][$value][$valueC]['errors'] = $frecuency[$attr][$value][$valueC]['errors'] + 1;
                }

                $frecuency[$attr][$value][$valueC]['total'] = $frecuency[$attr][$value][$valueC]['total'] + 1;
            }

            if(isset($frecuency[$attr][$value]['total']))
            {
                $frecuency[$attr][$value]['total'] = $frecuency[$attr][$value]['total'] + 1;
            }
            else
            {
                $frecuency[$attr][$value]['total'] = 1;
            }
        }

        $errors = array();
        $sum = 0;

        foreach($frecuency as $attr => $f)
        {
            foreach($f as $class => $value)
            {

                if(!isset($value['yes']) && isset($value['no']))
                {
                    $value['yes']['values'] = 0;
                }
                if(!isset($value['no']) && isset($value['yes']))
                {
                    $value['no']['values'] = 0;
                }

                if(!isset($value['>50K']) && isset($value['<=50K']))
                {
                    $value['>50K']['values'] = 0;
                }
                if(!isset($value['<=50K']) && isset($value['>50K']))
                {
                    $value['<=50K']['values'] = 0;
                }

                if(isset($value['yes']) && isset($value['no']))
                {
                    if(($value['yes']['values']) > ($value['no']['values']))
                    {
                        $errors[$class] = 'yes';
                    }
                    elseif(($value['yes']['values']) <= ($value['no']['values']))
                    {
                        $errors[$class] = 'no';
                    }
                }

                if(isset($value['>50K']) && isset($value['<=50K']))
                {
                    if(($value['>50K']['values']) > ($value['<=50K']['values']))
                    {
                        $errors[$class] = "'>50K'";
                    }
                    elseif(($value['>50K']['values']) <= ($value['<=50K']['values']))
                    {
                        $errors[$class] = "'<=50K'";
                    }
                }

            }

        }


        $result = '';
        foreach($errors as $key => $error)
        {
            $result .= "$key = $error <br/>";
        }
        return $result;
    }

    public function zeroR($attr)
    {
        $frecuency = [];
        $lastRow    = $this->sheet->getHighestRow();
        $column = $this->getColumn($attr);

        for ($row = 2; $row <= $lastRow; $row++)
        {
            $value = $this->sheet->getCell($column.$row)->getValue();
            $columns[] = $value;
            $this->setFrecuency($frecuency, $value, $attr, 'all');
        }

        $f = array();
        foreach ($frecuency as $key => $value)
        {
            $f[$key] = $value['value'];
        }

        $max = max($f);
        $result = "";
        foreach ($f as $key => $value)
        {
            if($value === $max)
            {
                $result .= "$key, ";
            }
        }

        return $result;
    }

}

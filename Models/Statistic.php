<?php

require_once("Reader.php");

class Statistic
{

    public $sheet;

    public function __construct($filename)
    {
        $this->sheet = new Reader($filename);
    }

    public function getFilters()
    {
        return $this->sheet->getAttributes();
    }

    /**
    * @param String $column The column to read
    * @param String $sort the sort type(ASC|DESC)
    * @param String[] $statisctis the statistics to show(fashion,media,mediana,max.min) for default its all
    **/
    public function getStatistics($column, $show, $sort = 'ASC', $statistics = 'all')
    {
        return $this->sheet->getData($column, $sort, $statistics, $show);
    }

    public function getInfo($filter, $show)
    {
        $data = [];
        if($filter === 'all')
        {
            $array = $this->sheet->getAttributes();
        }
        else
        {
            $array = $filter;
        }

        foreach($array as $attr)
        {
            $data[$attr] = $this->getStatistics($attr, $show);

        }
        return $data;
    }

    public function getConfiguration()
    {
        return $this->sheet->getConfiguration();
    }

    public function summation($array, $filters)
    {
        //$array_result = $array[$filters[0] .','. $filters[1]];
        $total        = array_sum($array);
        $lastRow      = $this->sheet->getHighestRow();
        $column1      = $this->sheet->getColumn($filters[0]);
        $column2      = $this->sheet->getColumn($filters[1]);
        $sum          = 0;


        foreach($array as $key => $value)
        {
            $excpected = $this->getTotal(explode(',', $key)[0], $array);
            $excpected = $excpected * $this->getTotal(explode(',', $key)[1], $array);
            $excpected = $excpected / $total;

            $result = (pow($value - $excpected, 2)) / $excpected;

            $sum += $result;
        }

        return $sum;
    }

    public function getTotal($attr, $array)
    {
        $sum = 0;
        foreach($array as $key => $value)
        {
            $index = explode(',', $key);

            if(in_array($attr, $index))
            {
                $sum += $value;
            }
        }

        return $sum;
    }

    public function getCorrelation($filters)
    {
        if($filters === 'all')
        {
            $f = $this->sheet->getAttributes();
            $filters = [];

            foreach($f as $key => $value)
            {
                $filters[] = $value;
            }
        }

        $correlations = [];
        $h = 0;
        $x = count($filters)-1;
        if($x === 0)
        {
            $x = count($filters);
            $filters[] = $filters;
            $h = 1;
        }
        $config = $this->getConfiguration();

        for($i=0; $i<=$x-$h; $i++)
        {
            for($j=0; $j<=$x-$h; $j++)
            {
                if(!is_null($filters[$j]) && !is_null($filters[$i]))
                {
                    if(trim($config[$filters[$i]]['type']) === 'numerico' && trim($config[$filters[$j]]['type']) === 'numerico')
                    {
                        $result      = $this->sheet->pearsonFunction($filters[$i], $filters[$j]);
                        $correlations[$filters[$i] .','. $filters[$j]]['value'] = $result;
                    }
                    else
                    {
                        $correlation = $this->sheet->getCorrelation($filters[$i], $filters[$j]);
                        $correlations[$filters[$i] .','. $filters[$j]]['value']  = $this->summation($correlation, $filters);
                    }
                }
            }
        }

        return $correlations;
    }

    public function getdegreesOfFreedom($array, $filters)
    {
        //$array_result = $array[$filters[0] .','. $filters[1]];
        $total        = array_sum($array);
        $lastRow      = $this->sheet->getHighestRow();
        $column1      = $this->sheet->getColumn($filters[0]);
        $column2      = $this->sheet->getColumn($filters[1]);
        $sum          = 0;

        foreach($array as $key => $value)
        {
            $excpected = $this->getTotal(explode(',', $key)[0], $array);
            $excpected = $excpected * $this->getTotal(explode(',', $key)[1], $array);
            $excpected = $excpected / $total;

            $result = (pow($value - $excpected, 2)) / $excpected;

            $sum += $result;
        }

        return $sum;
    }

    public function duplicateActualSheet($filename, $valuesToChange, $attrToChange)
    {
        $date = date('ymdGis');

        $folder = explode('.', $filename)[0];
        $backup = 'bak-' . $valuesToChange . '-' . $attrToChange . '_' . $date . '_' . $filename;

        if(file_exists('Files/' . $filename))
        {
            if(!file_exists('Files/' . $folder))
            {
                exec('mkdir Files/' . $folder);
                exec('chmod 777 -R Files/');
            }
            copy('Files/' . $filename, 'Files/' . $folder . '/' . $backup);

            return true;
        }
        else
        {
            return false;
        }
    }

    public function changeValues($valuesToChange, $attrToChange, $filename)
    {
        return $this->sheet->changeValues($valuesToChange, $attrToChange, $filename);
    }

    public function normalizeMinMax($attribute, $min, $max, $filename)
    {
        $this->duplicateActualSheet($filename, 'min_max', $attribute);
        $this->sheet->min_max($attribute, $min, $max, $filename);
    }

    public function normalizeZScore($attribute, $filename)
    {
        $this->duplicateActualSheet($filename, 'z_score_std', $attribute);
        $this->sheet->z_Score($attribute, $filename);
    }

    public function normalizeZScoreAbs($attribute, $filename)
    {
        $this->duplicateActualSheet($filename, 'z_score_abs', $attribute);
        $this->sheet->z_Score_Abs($attribute, $filename);
    }

    public function scalarDecimal($attribute, $filename)
    {
        $this->duplicateActualSheet($filename, 'escalamiento', $attribute);
        $this->sheet->escalamiento_decimal($attribute, $filename);
    }

    public function getOneR($attr)
    {
        return $this->sheet->oneR($attr);
    }

    public function getZeroR($attr)
    {
        return $this->sheet->zeroR($attr);
    }


}

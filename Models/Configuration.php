<?php

class Configuration
{
    private $fullname;
    private $configuration = [];

    public function __construct($prefijo)
    {
        $this->fullname = 'Configuration/' . $prefijo . '_configuration.txt';
        if(file_exists($this->fullname))
        {
            $this->setConfiguration();
            exec("chmod 777 $this->fullname");
        }
        else
        {
            fopen($this->fullname, "a");
            exec("chmod 777 $this->fullname");
        }
    }

    public function setConfiguration()
    {
        $file = fopen($this->fullname, 'r');

        while($line = fgets($file))
        {
            $array = explode('@', $line);

            $this->configuration[$array[0]]['value'] = $array[1];
            $this->configuration[$array[0]]['type'] = $array[2];
        }
        fclose($file);
    }

    public function getConfiguration()
    {
        return $this->configuration;
    }

    public function valid($column, $value)
    {
        if(isset($this->configuration[$column]))
        {
            ob_start();
            $value = preg_match($this->configuration[$column]['value'], $value);
            $error = error_get_last();
            ob_end_clean();
            if($error['message'] === 'preg_match(): Delimiter must not be alphanumeric or backslash')
            {
                echo 'Alguna expresion regular es incorrecta, revisalas por favor...';
                die();
            }
            else
            {
                return $value;
            }
        }
        return false;
    }
}

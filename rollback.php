<?php

$path = $_GET['filename'];

$dir = 'Files/' . $path;

$ignored = array('.', '..', '.svn', '.htaccess');

    $files = array();
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }

    arsort($files);
    $files = array_keys($files);

    if(count($files))
    {
        $rollback = $files[0];

        exec("rm Files/" . $path . '.xls');
        copy("Files/" . $path . '/' . $rollback, 'Files/' . $path . '.xls');
        exec("rm Files/" . $path . '/' . $rollback);
    }

    Header('Location: index.php');
?>

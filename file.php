<!DOCTYPE html>
<html>

    <head>
        <title>Cambiar archivo</title>

        <script>
            function handler(e){
                console.log(URL.createObjectURL(e.files[0]));
                console.log(e.files[0]);

                var form = document.getElementById('form');
                form.submit();
            }
        </script>
    </head>
    <body>
        <div>
            <form action="Models/File.php" method="POST" id="form">
                <input onChange="handler(this)" id="file" name="file" type="file" accept=".csv,.xls,.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
            </form>
        </div>
    </body>


</html>

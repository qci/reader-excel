<!DOCTYPE html>
<html>

    <head>
        <title>Configuracion</title>
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css" />
        <script src="node_modules/jquery/dist/jquery.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
        <style>

        .config label{
            font-weight: 600;
        }
        .title {
            text-align: center;
        }
        h1 {
            display: inline-block;
            margin-bottom: 0;
        }
        .config input{
            display: inline-block;
            width: 100%;
            border-radius: 4px;
            border: 1px solid #ccc;
            padding: 3px 4px;
            box-sizing: border-box;
        }

        .form-control.select {
            display: inline-block!important;
            width: auto;
            margin-bottom: 10px;
        }
        </style>
        <script>
            function sendIndex(){
                console.log('hola');
                window.location.href = window.location.origin;
            }
        </script>
    </head>
    <body>
        <div class="title">
            <h1>Lista de atributos</h1>
        </div>
        <?php
        require_once("Models/Statistic.php");
        $file_excel = '';
        $filename   = 'Configuration/' . "init.txt";
        if(file_exists($filename))
        {
            $file       = fopen($filename, "r");
            $file_excel = fread($file, 1024);
            fclose($file);
        }

        if(!empty($file))
        {
        $statistic = new Statistic($file_excel);
        $config = $statistic->getConfiguration();
        ?>
        <form action="config.php" method="POST">
        <?php
            foreach($statistic->getFilters() as $key => $filter)
            {
        ?>
                <div class="form-group">
                    <label class="col-xs-6" for="<?php echo $filter ?>"><?php echo $filter ?></label>
                    <select name="type[<?php echo $filter ?>]" class="form-control select">
                        <option value=""></option>
                        <option value="nominal" <?php echo (count($config) && isset($config[$filter]) && trim($config[$filter]['type']) == 'nominal') ? 'selected' : '' ?>>Nominal</option>
                        <option value="numerico" <?php echo (count($config) && isset($config[$filter]) && trim($config[$filter]['type']) == 'numerico') ? 'selected' : ''  ?>>Numerico</option>
                        <option value="clase" <?php echo (count($config) && isset($config[$filter]) && trim($config[$filter]['type']) == 'clase') ? 'selected' : ''  ?>>Clase</option>
                    </select>
                    <input id="<?php echo $filter ?>" class="form-control" name="new_data[<?php echo $filter ?>]" type="text" value="<?php echo (isset($config[$filter]['value'])) ? $config[$filter]['value'] : '' ?>" />
                </div>
        <?php
            }
            ?>
            <button type="submit" class="btn btn-success">Guardar</button>
            <a onclick="sendIndex()" class="btn btn-default">Regresar</a>
            </form>
            <?php
        }
        else
        {

        }
        ?>
    </body>

</html>

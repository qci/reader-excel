<?php

require_once('Models/Statistic.php');

$valuesToChange = $_POST['values'];
$attrToChange   = $_POST['attribute'];
$filename       = $_POST['filename'];

$statistic      = new Statistic($filename);

$result = $statistic->duplicateActualSheet($filename, $valuesToChange, $attrToChange);
if(!$result)
{
    die('El respaldo no se pudo guardar...');
}

$result = $statistic->changeValues($valuesToChange, $attrToChange, $filename);

Header("Location: index.php");


?>

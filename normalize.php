<?php

require_once('Models/Statistic.php');

$filename = $_GET['filename'];

$attr = $_GET['attr'];
$norm = $_GET['norm'];
if(isset($_GET['min']) && isset($_GET['max']))
{
    $min  = $_GET['min'];
    $max  = $_GET['max'];
}

$statistic      = new Statistic($filename);

if($norm === 'min_max')
{
    $statistic->normalizeMinMax($attr, $min, $max, $filename);
}
elseif($norm === 'z_score_std')
{
    $statistic->normalizeZScore($attr, $filename);
}
elseif($norm === 'z_score_abs')
{
    $statistic->normalizeZScoreAbs($attr, $filename);
}
elseif($norm === 'escalamiento')
{
    $statistic->scalarDecimal($attr, $filename);
}

Header('Location: index.php');


?>

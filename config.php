<?php

require_once("Models/Statistic.php");
$file_excel = '';
$filename   = 'Configuration/' . "init.txt";


if(file_exists($filename))
{
    $file       = fopen($filename, "r");
    $file_excel = fread($file, 1024);
    fclose($file);
}

if(!empty($file))
{
    $statistic = new Statistic($file_excel);
    $config = $statistic->getConfiguration();
    $filename = 'Configuration/' . explode('.', $file_excel)[0] . '_configuration.txt';

    $f = fopen($filename, "w");

    foreach($_POST['new_data'] as $key => $data)
    {
        $line = $key . '@' . $data . '@' . $_POST['type'][$key] . "\n";
        fwrite($f, $line);
    }

    fclose($f);

    header('Location: index.php');

}




?>

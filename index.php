<!DOCTYPE html>
<html>
    <?php
        require_once("Models/Statistic.php");
    ?>

    <head>

        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css" />
        <script src="node_modules/jquery/dist/jquery.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
        <title>lector de excel</title>

        <style>

            .title {
                text-align: center;
                border-bottom: 1px solid black;
                padding: 10px;
                margin-bottom: 10px;
            }
            h1 {
                margin: 0;
            }
            .correlation {
                display: inline-block;
                width: 100%;
                margin-top: 20px;
                border-radius: 4px;
                border: 1px solid #ccc;
            }
            .correlation p {
                font-size: 15px;
                margin: 0;
            }
            .correlation span {
                font-weight: 600;
            }
            h1 {
                display: inline-block;
                margin-bottom: 0;
            }

            .form {
                display: inline-block;
            }
            table {
                margin: auto;
            }

            .title-result {
                text-align: center;
                font-weight: 800;
            }
            .filters {
                margin-top: 40px;
                -webkit-transition: width 2s, height 4s;
            }
            .filters .filter {
                width: 16%;
                float: left;
                display: inline-block;
                border: 1px dashed black;
                text-align: center;
                position: relative;
                height: 24px;
                margin: 4px;
            }
            .filters .filter label {
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
            }

            .filter_custom {
                display: inline-block;
                width: 100%;
                margin-top: 10px;
                text-align: center;
            }

            .filter_custom label {
                border: 1px dashed black;
                width: 50%;
            }
            .filter_custom .checkbox {
                width: 41%;
                display: inline-block;
                border: 1px dashed black;
            }
            .btn-text {
                background: transparent;
                border: 0;
                text-decoration: underline;
                color: #0D85FF;

            }
            .table-container{
                border-top: 1px solid black;
                margin-top: 20px;
                width: 100%;
                display: inline-block;
            }

            .table-custom{
                display: inline-block;
                width: 33%;
                float: left;
            }

            .min-max {
                border: 1px solid #ccc;
                margin: 0 3px;
                border-radius: 4px;
            }
            .active {
                color: #0DBF34;
            }

        </style>

        <script>

            function handler(e){
                console.log(URL.createObjectURL(e.files[0]));
                console.log(e.files[0]);

                var form = document.getElementById('form');
                form.submit();
            }
            $(document).ready(function(){

                var input = document.getElementById('all');

                if(input.checked) {
                    check(input);
                }

            });
            function checkOne(input) {
                var label = document.getElementById('label-' + input.id);
                if(input.checked) {
                    label.setAttribute('class', 'active');
                } else {
                    label.setAttribute('class', '');
                }
            }

            function checkRadio(input) {

                if(input.id === 'errors') {
                    var another = document.getElementById('_all');
                } else {
                    var another = document.getElementById('errors');
                }

                label = document.getElementById('label-' + input.id);
                label.setAttribute('class', 'checkbox active');
                label = document.getElementById('label-' + another.id);
                label.setAttribute('class', 'checkbox');
                input.checked   = true;
                another.checked = false;
            }

            function rollback(file) {
                window.location.href = window.location.origin + '/rollback.php?filename=' + file;
            }

            function check(event) {

                var inputs = document.getElementsByName('filter[]');

                for(var i = 0; i < inputs.length; i ++) {
                    inputs[i].checked  = event.checked;
                    inputs[i].disabled = event.checked;
                    label = document.getElementById('label-' + inputs[i].id);
                    all = document.getElementById('label-all');
                    if(event.checked) {
                        label.setAttribute('class', 'active');
                        all.setAttribute('class', 'active');
                    } else {
                        label.setAttribute('class', '');
                        all.setAttribute('class', '');
                    }
                }

            }

            function unchecked(){
                var inputs = document.getElementsByTagName('input');

                for(var i = 0; i < inputs.length; i ++) {
                    inputs[i].removeAttribute('checked');
                    inputs[i].removeAttribute('disabled');
                }

                window.location.href = window.location.origin;
            }

            function showInput(link) {
                window.location.href = window.location.origin + '/file.php';
            }

            function hiddeInput(button) {
                var input = document.getElementById('filename');
                var link  = document.getElementById('change');

                button.setAttribute('type', 'hidden');
                input.setAttribute('type', 'hidden');
                link.setAttribute('type', 'hidden');
                button.submit();

            }
            function loadFile(){
                var f = document.getElementById('file');
                console.log(URL.createObjectURL(f.files[0]));
            }

            function hideFilters() {
                var filters = document.getElementById('filters');
                if(filters.style.display === 'none') {
                    filters.style.display = 'inherit';
                } else {
                    filters.style.display = 'none';
                }
            }
        </script>
    </head>
    <body>
        <?php
        $file_excel = '';
        $filename   = 'Configuration/' . "init.txt";
        if(file_exists($filename))
        {
            $file       = fopen($filename, "r");
            $file_excel = fread($file, 1024);
            fclose($file);
        }
        if(!empty($file_excel))
        {
        $statistic = new Statistic($file_excel);
        ?>
        <form>
            <div class="title">
                <h1><?php echo explode('.', $file_excel)[0] ?></h1>
                <br/>
                <button onclick="showInput(this)" type="button" class="btn btn-default">Cambiar archivo</button>
                <a class="btn btn-default" href="configuration.php">Configurar</a>
                <a class="btn btn-default" onclick="rollback('<?php echo explode('.', $file_excel)[0] ?>')">Rollback</a>
                <a onclick="hideFilters()" class="btn btn-default">Ocultar filtros</a>
            </div>
            <div class="filters" id="filters">
                <div class="col-xs-12">
                    <div class="col-xs-1">
                        <div class="form-group">
                            <label for="min">Minimo</label>
                            <input <?php echo (isset($_GET['min'])) ? 'disabled' : '' ?> id="min" name="min" class="min-max form-control" placeholder="min" value="<?php echo (isset($_GET['min'])) ? $_GET['min'] : '0' ?>" />
                        </div>
                    </div>
                    <div class="col-xs-1">
                        <div class="form-group">
                            <label for="max">Maximo</label>
                            <input <?php echo (isset($_GET['max'])) ? 'disabled' : '' ?> id="max" name="max" class="min-max form-control" placeholder="max" value="<?php echo (isset($_GET['max'])) ? $_GET['max'] : '1' ?>" />
                        </div>
                    </div>
                    <div>
                        Clase: <?php echo $statistic->sheet->class ?>
                    </div>
                </div>
                <div class="filter">
                    <input
                        id="all"
                        name="filter"
                        type="checkbox"
                        class="hidden"
                        <?php echo (isset($_GET['filter']) && $_GET['filter'] === 'all') ? 'checked' : '' ?>
                        onclick="check(this)"
                        value="all"/>
                    <label for="all" id="label-all" class="<?php echo (isset($_GET['filter']) && $_GET['filter'] === 'all') ? 'active' : '' ?>">
                        Todos
                    </label>
                </div>

                <?php
                    foreach($statistic->getFilters() as $key => $filter)
                    {
                    ?>
                    <div class="filter">
                        <input
                            id="<?php echo $filter ?>"
                            name="filter[]"
                            class="hidden"
                            onclick="checkOne(this)"
                            <?php echo ((isset($_GET['filter']) && $_GET['filter'] === 'all') || (isset($_GET['filter']) && in_array($filter, $_GET['filter']))) ? 'checked' : '' ?>
                            value="<?php echo $filter ?>"
                            type="checkbox" />
                        <label id="label-<?php echo $filter ?>" for="<?php echo $filter ?>" class="<?php echo ((isset($_GET['filter']) && $_GET['filter'] === 'all') || (isset($_GET['filter']) && in_array($filter, $_GET['filter']))) ? 'active' : '' ?>"><?php echo $filter ?></label>
                    </div>
                    <?php
                    }
                    ?>
                    <div class="filter_custom">
                        <label for="errors" class="checkbox <?php echo (isset($_GET['show']) && $_GET['show'] === 'errors' || isset($_GET['show']) && is_null($_GET['show'])) ? 'active' : '' ?>" id="label-errors">
                            <input
                                type="radio"
                                name="show"
                                onclick="checkRadio(this)"
                                class="hidden"
                                id="errors"
                                <?php echo (isset($_GET['show']) && $_GET['show'] === 'errors' || isset($_GET['show']) && is_null($_GET['show'])) ? 'checked' : '' ?>
                                value="errors" />
                            validar contra expresion regular
                        </label>
                        <label for="_all" class="checkbox <?php echo (isset($_GET['show']) && $_GET['show'] === 'all') ? 'active' : '' ?>" id="label-_all">
                            <input
                                type="radio"
                                name="show"
                                class="hidden"
                                onclick="checkRadio(this)"
                                <?php echo (isset($_GET['show']) && $_GET['show'] === 'all') ? 'checked' : '' ?>
                                id="_all"
                                value="all" />
                            todos los valores
                        </label>
                    </div>
                    <div class="filter_custom">
                        <input
                            type="checkbox"
                            name="correlacion"
                            id="correlacion"
                            class="hidden"
                            onclick="checkOne(this)"
                            <?php echo (isset($_GET['correlacion']) || isset($_GET['correlacion']) && is_null($_GET['correlacion'])) ? 'checked' : '' ?>
                            />
                        <label for="correlacion" id="label-correlacion" class="<?php echo (isset($_GET['correlacion']) || isset($_GET['correlacion']) && is_null($_GET['correlacion'])) ? 'active' : '' ?>">
                            mostrar correlacion
                        </label>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Validar</button>
                        <button type="reset" onclick="unchecked()" class="btn btn-default">reset</button>
                    </div>
                </div>
            </form>
            <?php

            if(isset($_GET['filter']) && isset($_GET['show']))
            {
                $results = $statistic->getInfo($_GET['filter'], $_GET['show']);
                $config = $statistic->getConfiguration();
            foreach($results as $title => $result)
            {
            if(count($result['frecuency']) || count($result['statistic']))
            {
            ?>
            <div class="table-container">
                <div class="title">
                    <strong>
                        <?php echo $title . ' (' . trim($config[$title]['type']) . ')' ?>
                        <br/>
                        <?php
                        if(trim($config[$title]['type']) === 'numerico')
                        {
                         ?>

                            <form class="form" method="GET" action="normalize.php">
                                <input name="filename" type="hidden" value="<?php echo $file_excel ?>" />
                                <input name="attr" type="hidden" value="<?php echo $title ?>" />
                                <input name="norm" type="hidden" value="min_max" />
                                <input name="min" type="hidden" value="<?php echo $_GET['min'] ?>" />
                                <input name="max" type="hidden" value="<?php echo $_GET['max'] ?>" />
                                <button class="btn btn-default">min-max</button>
                            </form>
                            <form class="form" method="GET" action="normalize.php">
                                <input name="filename" type="hidden" value="<?php echo $file_excel ?>" />
                                <input name="attr" type="hidden" value="<?php echo $title ?>" />
                                <input name="norm" type="hidden" value="z_score_std" />
                                <button class="btn btn-default">z-score (desviación std)</button>
                            </form>
                            <form class="form" method="GET" action="normalize.php">
                                <input name="filename" type="hidden" value="<?php echo $file_excel ?>" />
                                <input name="attr" type="hidden" value="<?php echo $title ?>" />
                                <input name="norm" type="hidden" value="z_score_abs" />
                                <button class="btn btn-default">z-score (media abs)</button>
                            </form>
                            <form class="form" method="GET" action="normalize.php">
                                <input name="filename" type="hidden" value="<?php echo $file_excel ?>" />
                                <input name="attr" type="hidden" value="<?php echo $title ?>" />
                                <input name="norm" type="hidden" value="escalamiento" />
                                <button class="btn btn-default">escalamiento decimal</button>
                            </form>
                            <?php
                        }
                        else
                        {
                            ?>
                            <form action="change_values.php" method="POST" class="form">
                                <input type="hidden" name="values" value="all"/>
                                <input type="hidden" name="attribute" value="<?php echo $title ?>"/>
                                <input type="hidden" name="filename" value="<?php echo $file_excel ?>"/>
                                <button type="submit" class="btn btn-default">
                                    cambiar todos
                                </button>
                            </form>
                            <?php
                        }
                        ?>
                    </strong>
                </div>
                <?php
                if(count($result['frecuency']))
                {
                ?>
                <div class="table-custom">
                    <div class="title-result">
                        Tabla de frecuencia
                    </div>
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Valor</th>
                                <th>frecuencia</th>
                                <?php
                                if(trim($config[$title]['type']) !== 'numerico'){
                                ?>
                                    <th>
                                        Posible valor
                                    </th>
                                <?php
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                    <?php
                    foreach($result['frecuency'] as $key => $value)
                    {
                    ?>
                        <tr>
                            <td><?php echo $key ?></td>
                            <td><?php echo $value['value'] ?></td>
                            <?php
                            if(trim($config[$title]['type']) !== 'numerico'){
                            ?>
                                <td>
                                    <?php
                                    if($value['sim'] !== '')
                                    {

                                    ?>
                                    <form action="change_values.php" method="POST">
                                        <input type="hidden" name="values" value="<?php echo $key ?>"/>
                                        <input type="hidden" name="attribute" value="<?php echo $title ?>"/>
                                        <input type="hidden" name="filename" value="<?php echo $file_excel ?>"/>
                                        <button type="submit" class="btn-text">
                                            <?php echo $value['sim'] ?>
                                        </button>
                                    </form>
                                    <?php
                                    }

                                    ?>
                                </td>
                            <?php
                            }
                        ?>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
                }
                if(count($result['statistic']))
                {
                ?>
                <div class="table-custom">
                    <div class="title-result">
                        Tabla estadistica
                    </div>
                    <table border="1">
                        <tbody>
                            <?php
                            foreach($result['statistic'] as $key => $value)
                            {
                            ?>
                                <tr>
                                    <td><?php echo $key ?></td>
                                    <?php
                                    if(!is_array($value))
                                    {
                                    ?>
                                    <td><?php echo $value ?></td>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <td>
                                    <?php
                                    foreach($value as $fashion)
                                    {
                                        echo $fashion . ', ';
                                    }
                                    ?>
                                    </td>
                                    <?php

                                    }

                                    ?>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                    </table>
                </div>
            <?php
                        }
                        ?>
            <div class="table-custom">
                <div class="title-result">
                    Algoritmos
                </div>
                <table border="1">
                    <tr>
                        <th>algoritmo</th>
                        <th>Resultado</th>
                    </tr>
                    <tr>
                        <td>ZeroR</td>
                        <td>
                            <?php
                                echo $statistic->getZeroR($title)
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>OneR</td>
                        <td><?php echo $statistic->getOneR($title) ?></td>
                    </tr>
                </table>
            </div>
    </div>

                <?php
            }
        }
    }

        if(isset($_GET['filter']))
        {
            $correlations = $statistic->getCorrelation($_GET['filter']);
            $dof = $statistic->sheet->degreeOfFreedom();
            if(isset($_GET['correlacion']) && $_GET['correlacion'] === 'on')
            {
            ?>
            <div class="correlation">
                <div class="title">
                    <strong>Tabla de correlación<span> (grados de libertad: <?php echo $dof ?>)</span></strong>
                </div>
                <?php
                arsort($correlations);
                foreach($correlations as $key => $value)
                {
                ?>
                    <p class="correlation">Correlación(<?php echo $key ?>): <span><?php echo $value['value'] ?></span></p>
                <?php
                }
                ?>
            </div>
            <?php
                }
            }
        }
        else
        {
        ?>
            <form action="Models/File.php" method="POST" id="form">
                <input onChange="handler(this)" id="file" name="file" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
            </form>
        <?php
        }
        ?>
    </body>

</html>
